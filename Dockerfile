FROM webdevops/php-nginx-dev:7.4
COPY ./platform /app
RUN ln -s /app/env/.env /app/.env
RUN composer1 install -d /app